package com.leapfrog.corebanking.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbConnection {
	private Connection conn=null;
	private PreparedStatement stmt=null;
	
	public void connect()throws ClassNotFoundException,SQLException{
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String connString = "jdbc:sqlserver://LENOVO-PC\\DIXANTA:1433;databaseName=AJV17004;integratedSecurity=true;";
		conn = DriverManager.getConnection(connString);
	}
	
	public PreparedStatement initStatement(String sql)throws SQLException{
		stmt=conn.prepareStatement(sql);
		return stmt;
	}
	
	public ResultSet query()throws SQLException{
		return stmt.executeQuery();
	}
	
	public int update()throws SQLException{
		return stmt.executeUpdate();
	}
	
	
	public void close()throws SQLException{
		if(conn!=null && !conn.isClosed()){
			conn.close();
			conn=null;
		}
	}

}
