package com.leapfrog.corebanking.controller;

import java.sql.SQLException;
import java.util.Scanner;

import com.leapfrog.corebanking.dao.AccountDAO;
import com.leapfrog.corebanking.entity.Account;

public class AccountController {
	private AccountDAO accountDAO;
	private Scanner input;

	public AccountController(AccountDAO accountDAO, Scanner input) {
		this.accountDAO = accountDAO;
		this.input = input;
	}

	private void menu() {
		System.out.println("1. Show All Accounts");
		System.out.println("2. Search By Id");
		System.out.println("3. Add Account");
		System.out.println("4. Edit Account");
		System.out.println("5. Delete Account");
		System.out.println("6. Exit");
		System.out.println("Enter Your Choice:");

	}

	private void showAllView() throws ClassNotFoundException, SQLException {
		for (Account a : accountDAO.getAll()) {
			System.out.println(a);
		}
	}

	private void seachView() throws ClassNotFoundException, SQLException {
		System.out.println("Enter Id to Search:");
		int id = input.nextInt();
		Account account = accountDAO.getById(id);
		if (account != null) {
			System.out.println(account);
		} else {
			System.out.println("Record Not Found");
		}
	}

	private void addView() throws ClassNotFoundException, SQLException {
		while (true) {
			Account account = new Account();
			System.out.println("Enter Account Name:");
			account.setName(input.nextLine());
			System.out.println("Enter Account Description:");
			account.setDescription(input.nextLine());
			System.out.println("Enter Account Interest:");
			account.setInterest(input.nextDouble());
			System.out.println("Enter Account Maturity:");
			account.setMaturity(input.nextInt());
			System.out.println("Enter Account Minimum Balance:");
			account.setMinumumBalance(input.nextInt());
			System.out.println("Enter Account Status:");
			account.setStatus(input.nextBoolean());

			accountDAO.insert(account);
			System.out.println("do you want to add more [Y/N]:");
			if (input.next().equalsIgnoreCase("n")) {
				break;
			}
		}
	}

	public void process() throws ClassNotFoundException, SQLException {
		menu();
		switch (input.nextInt()) {
		case 1:
			showAllView();
			break;
		case 2:
			seachView();
			break;
		case 3:
			addView();
			break;
		case 6:
			System.exit(0);
			break;
		}
	}

}
