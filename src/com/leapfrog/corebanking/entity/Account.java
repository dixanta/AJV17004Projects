package com.leapfrog.corebanking.entity;

import java.util.Date;

public class Account {
	private int id;
	private String name,description;
	private double interest;
	private int minumumBalance,maturity;
	private Date addedDate,ModifedDate;
	private boolean status;
	
	public Account(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public int getMinumumBalance() {
		return minumumBalance;
	}

	public void setMinumumBalance(int minumumBalance) {
		this.minumumBalance = minumumBalance;
	}

	public int getMaturity() {
		return maturity;
	}

	public void setMaturity(int maturity) {
		this.maturity = maturity;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Date getModifedDate() {
		return ModifedDate;
	}

	public void setModifedDate(Date modifedDate) {
		ModifedDate = modifedDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", name=" + name + ", description=" + description + ", interest=" + interest
				+ ", minumumBalance=" + minumumBalance + ", maturity=" + maturity + ", addedDate=" + addedDate
				+ ", ModifedDate=" + ModifedDate + ", status=" + status + "]";
	}
	
	
	
	
}
