package com.leapfrog.corebanking.dao;

import java.sql.SQLException;
import java.util.List;

import com.leapfrog.corebanking.entity.Account;

public interface AccountDAO {
	List<Account> getAll()throws ClassNotFoundException,SQLException;
	Account getById(int id)throws ClassNotFoundException,SQLException;
	int insert(Account account)throws ClassNotFoundException,SQLException;
	int update(Account account)throws ClassNotFoundException,SQLException;
	int delete(int id)throws ClassNotFoundException,SQLException;
}
