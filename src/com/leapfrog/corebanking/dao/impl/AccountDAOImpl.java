package com.leapfrog.corebanking.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.leapfrog.corebanking.dao.AccountDAO;
import com.leapfrog.corebanking.dbutil.DbConnection;
import com.leapfrog.corebanking.entity.Account;

public class AccountDAOImpl implements AccountDAO {
	private DbConnection db=new DbConnection();

	private Account mapData(ResultSet rs)throws SQLException{
		Account account = new Account();
		account.setId(rs.getInt("Id"));
		account.setName(rs.getString("AccountName"));
		account.setDescription(rs.getString("AccountDescription"));
		account.setInterest(rs.getDouble("Interest"));
		account.setMaturity(rs.getInt("Maturity"));
		account.setMinumumBalance(rs.getInt("MinimumBalance"));
		account.setAddedDate(rs.getDate("AddedDate"));
		account.setModifedDate(rs.getDate("ModifiedDate"));
		account.setStatus(rs.getBoolean("Status"));
		return account;
	}
	@Override
	public List<Account> getAll() throws ClassNotFoundException, SQLException {
		List<Account> accounts = new ArrayList<>();
		db.connect();
		String sql = "SELECT * FROM Accounts";
		db.initStatement(sql);
		ResultSet rs = db.query();
		while (rs.next()) {
			accounts.add(mapData(rs));
		}

		db.close();
		return accounts;
	}

	@Override
	public Account getById(int id) throws ClassNotFoundException, SQLException {
		Account account = null;

		db.connect();
		String sql = "SELECT * FROM Accounts WHERE Id=?";
		PreparedStatement stmt = db.initStatement(sql);
		stmt.setInt(1, id);
		ResultSet rs = db.query();

		if (rs.next()) {
			account = mapData(rs);
		}

		db.close();

		return account;
	}
	@Override
	public int insert(Account account) throws ClassNotFoundException, SQLException {
		String sql="INSERT INTO Accounts(AccountName,AccountDescription," 
					+"Interest,Maturity,MinimumBalance,Status) VALUES(?,?,?,?,?,?)";
		db.connect();
		PreparedStatement stmt= db.initStatement(sql);
		stmt.setString(1,account.getName());
		stmt.setString(2,account.getDescription());
		stmt.setDouble(3,account.getInterest());
		stmt.setInt(4,account.getMaturity());
		stmt.setInt(5,account.getMinumumBalance());
		stmt.setBoolean(6,account.isStatus());
		int result=db.update();
		db.close();
		
		return result;
	}
	@Override
	public int update(Account account) throws ClassNotFoundException, SQLException {
		String sql="UPDATE Accounts set AccountName=?,AccountDescription=?," 
				+"Interest=?,Maturity=?,MinimumBalance=?,Status=?,ModifiedDate=getDate() "
				+" WHERE Id=?";
	db.connect();
	PreparedStatement stmt= db.initStatement(sql);
	stmt.setString(1,account.getName());
	stmt.setString(2,account.getDescription());
	stmt.setDouble(3,account.getInterest());
	stmt.setInt(4,account.getMaturity());
	stmt.setInt(5,account.getMinumumBalance());
	stmt.setBoolean(6,account.isStatus());
	stmt.setInt(7,account.getId());
	int result=db.update();
	db.close();
	
	return result;
	}
	@Override
	public int delete(int id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

}
