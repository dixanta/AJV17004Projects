package com.leapfrog.corebanking;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.leapfrog.corebanking.controller.AccountController;
import com.leapfrog.corebanking.dao.AccountDAO;
import com.leapfrog.corebanking.dao.impl.AccountDAOImpl;
import com.leapfrog.corebanking.entity.Account;



public class Program {

	public static void main(String[] args) {		
		try{
			
			Scanner input=new  Scanner(System.in);
			AccountController ac=new AccountController(new AccountDAOImpl(),input);
			while(true){
				
				ac.process();
			}
						
		}catch(ClassNotFoundException | SQLException ce){
			System.out.println(ce.getMessage());
		}
	}

}
